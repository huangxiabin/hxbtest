package com.weizhong.yiwan.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.weizhong.yiwan.R;


/**
 * Created by yzn on 2016/11/8.
 * Glide图片加载
 */

public class GlideImageLoadUtils {

    //new RequestListener<String, Bitmap>() {
//        @Override
//        public boolean onException(Exception e, String s, Target<Bitmap> target, boolean b) {
//            Log.e("TAG","加载Path图片失败 = " + s + "  mssage = " + e.getMessage());
//            if(e != null){
//                e.printStackTrace();
//            }
//            return false;
//        }
//
//        @Override
//        public boolean onResourceReady(Bitmap bitmap, String s, Target<Bitmap> target, boolean b, boolean b1) {
//            return false;
//        }
//    }
    //显示本地文件图片
    public static void displayFileImage(Context context, String filePath, ImageView imageView, int defaultRes) {
        if (null == context) return;
        if (imageView == null) return;
        if (context instanceof Activity && ((Activity) context).isFinishing()) return;
        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .placeholder(defaultRes)
                .error(defaultRes);

        Glide.with(context)
                .asBitmap()
                .load(filePath)
                .apply(options)
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        Log.e("TAG", "加载Path图片失败 = " + model.toString() + "  mssage = " + e.getMessage());
                        if (e != null) {
                            e.printStackTrace();
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                }).into(imageView);
    }

    @SuppressLint("CheckResult")
    public static void displayImage(final Context context, String url, final ImageView imageView, int defaultRes) {
        if (null == context) return;
        if (context instanceof Activity && ((Activity) context).isFinishing()) return;
        if (url == null) url = "";
        if (imageView == null) return;
        RequestOptions options = new RequestOptions()
                .error(defaultRes)
                .placeholder(defaultRes);
        if (url.contains(".gif")) {
            options.diskCacheStrategy(DiskCacheStrategy.DATA);
            Glide.with(context)
                    .asGif()//
                    .apply(options)
                    .load(url).into(imageView);
        } else {

            options.diskCacheStrategy(DiskCacheStrategy.ALL);
            Glide.with(context)
                    .asBitmap()//会调用ImageView 的 setImageBitmap（）；
                    .load(url)
                    .apply(options)
                    .into(imageView);
        }

    }


    public static void displayCircleImage(final Context context, String url, final ImageView imageView, float width, int color, int defaultRes) {
        if (null == context) return;
        if (context instanceof Activity && ((Activity) context).isFinishing()) return;
        if (null == imageView) return;
        if (url == null) url = "";
        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(defaultRes)
                .transform(new GlideCircleTransform(context, width, color))
                .placeholder(defaultRes);
        Glide.with(context)
                .load(url)
                .apply(options)
                .into(imageView);
    }


    /**
     * 毛玻璃(高斯模糊)效果
     *
     * @param context
     * @param url
     * @param imageView
     * @param radius     模糊程度
     * @param defaultRes
     */
    public static void displayBlurImage(final Context context, String url, final ImageView imageView, int radius, int defaultRes) {
//        Glide.with(context).load(url).bitmapTransform(new BlurTransformation(context, radius)).into(imageView);
        if (null == context) return;
        if (context instanceof Activity && ((Activity) context).isFinishing()) return;
        if (null == imageView) return;
        if (url == null) url = "";
        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(defaultRes)
                .optionalTransform(new GlideBlurTransformation(context, radius))
                .placeholder(defaultRes);

        Glide.with(context)
                .load(url)
                .apply(options)
                .into(imageView);
    }


    public static void rotateDisplayImage(final Context context, String url, final ImageView imageView, int defaultRes, float rotateRotationAngle) {
        if (null == context) return;
        if (context instanceof Activity && ((Activity) context).isFinishing()) return;
        if (null == imageView) return;
        if (url == null) url = "";

        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH)
                .error(getIconNormalOptions())
                .transform(new RotateTransformation(context, rotateRotationAngle))
                .placeholder(defaultRes);

        Glide.with(context)
                .asBitmap()
                .load(url)
                .apply(options)
                .into(imageView);
    }


    //    new SimpleTarget<Bitmap>() {
//        @Override
//        public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
//            if (onLoadImageListener != null) {
//                onLoadImageListener.onSucess(bitmap, url);
//            }
//        }
//
//        @Override
//        public void onLoadFailed(Exception e, Drawable errorDrawable) {
//            super.onLoadFailed(e, errorDrawable);
//            if (onLoadImageListener != null) {
//                onLoadImageListener.onFail(errorDrawable);
//            }
//        }
//
//    }
    public static void loadImage(Context activity, final String url, final OnLoadImageListener onLoadImageListener) {
        if (null == activity) return;
        if (activity instanceof Activity && ((Activity) activity).isFinishing()) return;
        if (TextUtils.isEmpty(url)) return;

        RequestOptions options = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.ALL);

        Glide.with(activity)
                .asBitmap()
                .load(url)
                .apply(options)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        if (onLoadImageListener != null) {
                            onLoadImageListener.onSucess(resource, url);
                        }
                    }
                });
    }

    //显示本地资源图片
    public static void loadLocalImage(Context context, int res, ImageView imageView, int defaultRes) {
        if (null == context) return;
        if (context instanceof Activity && ((Activity) context).isFinishing()) return;
        if (null == imageView) return;

        RequestOptions options = new RequestOptions()
                .error(getIconNormalOptions())
                .placeholder(defaultRes)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE);

        Glide.with(context)
                .asBitmap()
                .load(res)
                .apply(options)
                .into(imageView);
    }


    public static int getIconNormalOptionsNoCacheOnDisc() {
        return R.drawable.shape_rectangular_image;
    }

    public static int getIconNormalOptions() {
        return R.drawable.shape_rectangular_image;
    }

    public static int getUserNormalOptions() {
        return R.mipmap.stand_alone_img;
    }

    public static int getUserHeadDef() {
        return R.mipmap.my_head_def;
    }


    public static int getGameIconOptions() {
        return R.drawable.shape_square_corner_image;
    }

    public static int getVideoFrameOptions() {
        return R.drawable.shape_white_rectangular_image;
    }

    public static int getCirecleOptions() {
        return R.drawable.circle_default;
    }

    public static int getCircleUserOptions() {
        return R.mipmap.home_member_icon;
    }

    public interface OnLoadImageListener {
        void onSucess(Bitmap bitmap, String url);

        void onFail(Drawable errorDrawable);
    }

    public static void clear(Context context, View view) {
        Glide.with(context).clear(view);
    }

}
